# Remote Control plugin language source file.
# Copyright (C) 2007 Oliver Endriss <o.endriss@gmx.de>
# This file is distributed under the same license as the Remote Control plugin package.
# Waldemar Nikel, 2006
#
msgid ""
msgstr ""
"Project-Id-Version: Remote Control plugin 0.4.0\n"
"Report-Msgid-Bugs-To: <o.endriss@gmx.de>\n"
"POT-Creation-Date: 2015-09-15 18:54+0200\n"
"PO-Revision-Date: 2007-10-05 06:58+0200\n"
"Last-Translator: Waldemar Nikel, 2006\n"
"Language-Team: <vdr@linuxtv.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-5\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Remote control"
msgstr "����� ����������"

#, c-format
msgid "%s: %s"
msgstr "%s: %s"

msgid "Error uploading keymap"
msgstr "������ ��� �������� ���������"

msgid "Press any key to use pre-loaded keymap"
msgstr "������� ����� ������ ��� ������������� ����� ���������� ���������"

msgid "User-supplied keymap will be used"
msgstr "����� �������������� ��������� ������������"

msgid "Remote control test - press and hold down any key"
msgstr "�������� ������ - ������� � �������� ����� ������"

msgid "RC5 protocol detected"
msgstr "������� �������� RC5"

msgid "RC5 protocol detected (inverted signal)"
msgstr "������� �������� RC5 (� ��������������� �������)"

msgid "RCMM protocol detected"
msgstr "������� �������� RCMM"

msgid "RCMM protocol detected (inverted signal)"
msgstr "������� �������� RCMM (� ��������������� �������)"

msgid "No remote control detected"
msgstr "����� ���������� �� �������"
